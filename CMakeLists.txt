cmake_minimum_required(VERSION 3.12)
project(libft C)

set(CMAKE_C_STANDARD 99)

aux_source_directory(./sources/ft_memory MEMORY)
aux_source_directory(./sources/ft_printf/src PRINTF)
aux_source_directory(./sources/ft_string STRING)
aux_source_directory(./sources/ft_list LIST)
aux_source_directory(./sources/ft_sys SYS)

add_library(ft STATIC ${MEMORY} ${STRING} ${PRINTF} ${LIST} ${SYS})

target_include_directories(ft PUBLIC ./includes)

include_directories(./libft/includes)

add_executable(test main.c)
target_link_libraries(test ft)
