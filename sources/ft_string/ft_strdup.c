/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 15:22:41 by vkryvono          #+#    #+#             */
/*   Updated: 2017/11/24 19:59:01 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strdup(const char *s1)
{
	char	*str;
	size_t	i;
	size_t	n;

	i = 0;
	n = ft_strlen(s1) + 1;
	if (!(str = (char*)malloc(n * sizeof(char))))
		return (NULL);
	while (i < n)
	{
		str[i] = s1[i];
		i++;
	}
	return (str);
}
