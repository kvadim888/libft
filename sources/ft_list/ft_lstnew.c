/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 01:51:15 by vkryvono          #+#    #+#             */
/*   Updated: 2017/11/25 12:38:46 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, intmax_t content_size)
{
	t_list *node;

	node = malloc(sizeof(t_list));
	if (node == NULL)
		return (NULL);
	node->next = NULL;
	if (content == NULL)
	{
		node->content_size = 0;
		node->content = NULL;
	}
	else
	{
		node->content_size = content_size;
		if ((node->content = malloc((size_t)content_size)) == NULL)
		{
			free(node);
			return (NULL);
		}
		ft_memcpy(node->content, content, (size_t)content_size);
	}
	return (node);
}
