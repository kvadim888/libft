/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 03:07:18 by vkryvono          #+#    #+#             */
/*   Updated: 2017/11/23 21:03:39 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*head;
	t_list	*node;
	t_list	*buf;

	head = NULL;
	if (lst == NULL)
		return (NULL);
	if ((*f)(lst) != NULL)
	{
		buf = (*f)(lst);
		node = ft_lstnew(buf->content, buf->content_size);
		head = node;
		while (lst->next != NULL)
		{
			buf = (*f)(lst->next);
			node->next = ft_lstnew(buf->content, buf->content_size);
			lst = lst->next;
			node = node->next;
		}
	}
	return (head);
}
