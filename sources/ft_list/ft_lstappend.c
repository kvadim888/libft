/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstappend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 20:51:20 by vkryvono          #+#    #+#             */
/*   Updated: 2018/08/16 21:16:05 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstappend(t_list *lst, t_list *new)
{
	t_list *tmp;

	if (!lst && new)
		return (new);
	if (lst && new)
	{
		tmp = lst;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
		tmp = tmp->next;
		tmp->next = NULL;
	}
	return (lst);
}
