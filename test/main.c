/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 11:54:09 by vkryvono          #+#    #+#             */
/*   Updated: 2018/11/21 02:17:38 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include "libft.h" 

int	main(int ac, char **av)
{
	int		i;
	int		fd;
	char	*line;

	if (ac == 0)
		return (0);
	i = 1;
	while (i < ac)
	{
		fd = open(av[i], O_RDONLY);
		while (get_next_line(fd, &line) > 0)
		{
			printf("%s\n", line);
			ft_strdel(&line);
		}
		close(fd);
		i++;
	}
	return (0);
}
