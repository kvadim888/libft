/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkryvono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 11:54:09 by vkryvono          #+#    #+#             */
/*   Updated: 2018/11/20 06:48:27 by vkryvono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include "libft.h" 

int		main()
{
	char *line;

	while (get_next_line(STDIN_FILENO, &line) > 0)
		printf(">%s\n", line);
	ft_strdel(&line);
	return (0);
}